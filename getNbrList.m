function [list] = getNbrList(lvl_idx, box_idx)
%lvl_idx is level index of box whose nbr list is queried
%box_idx is the quad tree index at that level

list = zeros(1, 8);
%list = [n, nne, e, sse, s, ssw, w, nnw]

cartesian_idx = getCartesianIdx(lvl_idx, box_idx);
i = cartesian_idx(1);
j = cartesian_idx(2);

list(1) = getQuadTreeIdx(lvl_idx, [i j + 1]);
list(2) = getQuadTreeIdx(lvl_idx, [i + 1, j + 1]);
list(3) = getQuadTreeIdx(lvl_idx, [i + 1, j]);
list(4) = getQuadTreeIdx(lvl_idx, [i + 1, j - 1]);
list(5) = getQuadTreeIdx(lvl_idx, [i, j - 1]);
list(6) = getQuadTreeIdx(lvl_idx, [i - 1, j - 1]);
list(7) = getQuadTreeIdx(lvl_idx, [i - 1, j]);
list(8) = getQuadTreeIdx(lvl_idx, [i - 1, j + 1]);

if j == 1
  list(4) = 0;
  list(5) = 0;
  list(6) = 0;
end

if i == 1
  list(6) = 0;
  list(7) = 0;
  list(8) = 0;
end

if i == 2^((lvl_idx) - 1)
  list(2) = 0;
  list(3) = 0;
  list(4) = 0;
end

if j == 2^((lvl_idx) - 1)
  list(8) = 0;
  list(1) = 0;
  list(2) = 0;
end

list = list(list ~= 0);

end
