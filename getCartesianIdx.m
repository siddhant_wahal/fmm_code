function [idx] = getCartesianIdx(lvl_idx, box_idx)

idx = zeros(1, 2);
coords = getCornerCoords(lvl_idx, box_idx);

idx(1) = coords(1, 1) * 2^(lvl_idx - 1) + 1;
idx(2) = coords(1, 2) * 2^(lvl_idx - 1) + 1;

end
