 clc
clear all

N = 1;
%N = 10;
eps = 1e-12;

num_levels = 2;
p = 10;


loc_particles = [0.35 0.35];

q_max = 10; %maximum possible charge on a particle, in coulombs.

charge = 1.0;

for lvl_idx = 1 : num_levels - 1
  for box_idx = 1 : 4^(lvl_idx - 1)
    level(lvl_idx).box(box_idx) = struct('psi', zeros(1, p), 'psi_tilde', zeros(1, p), 'phi', zeros(1, p + 1), 'nbr_list', zeros(1, 8), 'i_list', zeros(1, 27));
    %phi is of length p + 1 to accomodate phi(1) = Q = sum(charge(1 : N))
  end
end

for box_idx = 1 : 4^(num_levels - 1)
  level(num_levels).box(box_idx) = struct('psi', zeros(1, p), 'psi_tilde', zeros(1, p), 'phi', zeros(1, p + 1), 'nbr_list', zeros(1, 8), 'i_list', zeros(1, 27), 'particles_list', zeros(1, 10));
end

for box_idx = 1 : 4^(num_levels - 1)
  level(num_levels).box(box_idx).particles_list = getParticlesList(box_idx, num_levels, loc_particles); 
end

for lvl_idx = 2 : num_levels
  for box_idx = 1 : 4^(lvl_idx - 1)
    level(lvl_idx).box(box_idx).nbr_list = getNbrList(lvl_idx, box_idx);
  end
end

for lvl_idx = 2 : num_levels
  for box_idx = 1 : 4^(lvl_idx - 1)
    level(lvl_idx).box(box_idx).i_list = getInteractionList(lvl_idx, box_idx);
  end
end

%construct multipole expansions for the finest level

for box_idx = 1 : 4^(num_levels - 1)
  level(num_levels).box(box_idx).phi = constructRootMultipoleExp(p, num_levels, loc_particles, charge, box_idx);
end

%upward pass

for lvl_idx = num_levels - 1 : - 1 : 1
  for box_idx = 1 : 4^(lvl_idx - 1)
    children = getChildren(box_idx);
    level(lvl_idx).box(box_idx).phi = shiftMultipoleExp(level, lvl_idx + 1, children(1)) + shiftMultipoleExp(level, lvl_idx + 1, children(2)) + shiftMultipoleExp(level, lvl_idx + 1, children(3)) + shiftMultipoleExp(level, lvl_idx + 1, children(4));
  end
end



