function [coords] = getCornerCoords(lvl_idx, box_idx)% bl, br, tl, tr
if (lvl_idx == 1)
  coords = [0, 0; 
            1, 0; 
            0, 1;
            1, 1];
  return
else
  parent_coords = getCornerCoords(lvl_idx - 1, getParent(box_idx));
  switch(mod(box_idx, 4))
   case 1
     coords(1, 1) = parent_coords(1, 1); 
     coords(1, 2) =  parent_coords(1, 2);
     
   case 2
     coords(1, 1) = parent_coords(1, 1) + 1/2^(lvl_idx - 1);
     coords(1, 2) = parent_coords(1, 2);

   case 3
     coords(1, 1) = parent_coords(1, 1);
     coords(1, 2) = parent_coords(1, 2) + 1/2^(lvl_idx - 1);

   case 0
     coords(1, 1) = parent_coords(1, 1) + 1/2^(lvl_idx - 1);
     coords(1, 2) = parent_coords(1, 2) + 1/2^(lvl_idx - 1);
  end

  coords(2, 1) = coords(1, 1) + 1/2^(lvl_idx - 1);
  coords(2, 2) = coords(1, 2);
  coords(3, 1) = coords(1, 1); 
  coords(3, 2) = coords(1, 2) + 1/2^(lvl_idx - 1);
  coords(4, 1) = coords(1, 1) + 1/2^(lvl_idx - 1); 
  coords(4, 2) = coords(1, 2) + 1/2^(lvl_idx - 1); 

end
end
