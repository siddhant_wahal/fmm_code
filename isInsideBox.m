function return_val = isInsideBox(num_levels, box_idx, loc_particles, particle_idx)

coords = getCornerCoords(num_levels, box_idx);
left = coords(1, 1);
right = coords(2, 1);
top = coords(3, 2);
bottom = coords(1, 2);

x = loc_particles(particle_idx, 1);
y = loc_particles(particle_idx, 2);

if (x >= left && x <= right)
  if (y >= bottom && y <= top)
    return_val = 1;
  else 
    return_val = 0;
  end
else 
  return_val = 0;
end
end
