function [child_idx] = getChildren(i)

child_idx = [4*i - 3, 4*i - 2, 4*i - 1, 4*i];

end
