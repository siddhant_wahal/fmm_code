function [list] = getInteractionList(lvl_idx, box_idx)

%get box_idx's parent's nearest neighbours
%get their children
%check which children are well separated, or, remove non-well-separated boxes
%consolidate list
box_coord = getCartesianIdx(lvl_idx, box_idx);

list = zeros(1, 32);
parents_nbr_list = getNbrList(lvl_idx - 1, getParent(box_idx));

count1 = 1;
for i = 1 : length(parents_nbr_list)
  list(count1 : count1 + 3) = getChildren(parents_nbr_list(i));
  count1 = count1 + 4;
end

list = list(list ~= 0);


for j = 1 : length(list)
  nbr_coord = getCartesianIdx(lvl_idx, list(j));
  if ((abs(nbr_coord(1) - box_coord(1)) <= 1) && (abs(nbr_coord(2) - box_coord(2)) <= 1))
    list(j) = -1;
  end
  j = j + 1;
end
list = list(list ~= -1);
end
