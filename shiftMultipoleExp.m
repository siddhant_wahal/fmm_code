%Using function binomial(n, k) written by Mukhrar Ullat, University of Rostock. Usage under BSD license.
function [expr] = shiftMultipoleExp(level, lvl_idx, box_idx)

box_coords = getCornerCoords(lvl_idx, box_idx);
parent_coords = getCornerCoords(lvl_idx - 1, getParent(box_idx));

box_center = complex(0.5*(box_coords(1, 1) + box_coords(4, 1)), 0.5*(box_coords(1, 2) + box_coords(4, 2)));
parent_center = complex(0.5*(parent_coords(1, 1) + parent_coords(4, 1)), 0.5*(parent_coords(1, 2) + parent_coords(4, 2)));

z0 = box_center - parent_center;

multipole_expr = level(lvl_idx).box(box_idx).phi;
expr = zeros(1, length(multipole_expr));

expr(1) = multipole_expr(1);

for l = 2 : length(expr)
   expr(l) = - expr(1) * (z0^(l - 1)) / (l - 1) + sum((multipole_expr(2 : l) .* (z0.^(l - 2 : -1 : 0))) .* binomial(l - 2, 0 : l - 2));
end

end
