function [expr] = constructRootMultipoleExp(p, num_levels, loc_particles, charge, box_idx)

particles_list = getParticlesList(num_levels, box_idx, loc_particles);
complex_loc = complex(loc_particles(particles_list(1 : end), 1), loc_particles(particles_list(1 : end), 2));
charge_list = charge(particles_list(1 : end));

expr = zeros(1, p + 1);

expr(1) = sum(charge_list);

for k = 2 : p + 1
  expr(k) = (-1 / (k - 1)) * sum(charge_list .* ((complex_loc.') .^ (k - 1))); %theorem 1, had to take the non conjugate transpose of complex_loc for consistency
end

end
