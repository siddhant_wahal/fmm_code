function [idx] = getQuadTreeIdx(lvl_idx, cartesian_idx)

i = cartesian_idx(1);
j = cartesian_idx(2);
parent_cartesian_idx = [floor((i - 1) / 2) + 1 floor((j - 1) / 2) + 1];
%idx = floor((j - 1) / 2) * 2^(lvl_idx - 2) * 4 + floor((i - 1) / 2) * 4 + 2 * mod(i - 1, 2) + mod(j - 1, 2) + 1;
if lvl_idx == 1
  idx = 1;
  return
else
 idx = 4 * (getQuadTreeIdx(lvl_idx - 1, parent_cartesian_idx) - 1) + 2 * ((j - 2 * (parent_cartesian_idx(2) - 1) - 1)) + (i - 2 * (parent_cartesian_idx(1) - 1));
end

end
