function [arr] = getParticlesList(num_levels, box_idx, loc_particles)

temp = size(loc_particles);
arr = zeros(1, 10);
k = 1;
for particle_idx = 1 : temp(1);
  if (isInsideBox(num_levels, box_idx, loc_particles, particle_idx) == 1)
    arr(k) = particle_idx;
    k = k + 1;
  end
end

arr = arr(arr ~= 0);

end
